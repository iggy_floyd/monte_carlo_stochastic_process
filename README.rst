================================================================
 Application of the Monte-Carlo techniques in Option Trading
================================================================





:Author: Igor Marfin
:Contact: igor.marfin@unister.de
:Organization: private
:Date: Apr 14, 2016, 9:06:51 AM
:Status: draft
:Version: 1
:Copyright: This document has been placed in the public domain. You
            may do with it as you wish. You may copy, modify,
            redistribute, reattribute, sell, buy, rent, lease,
            destroy, or improve it, quote it at length, excerpt,
            incorporate, collate, fold, staple, or mutilate it, or do
            anything else to it that your or anyone else's heart
            desires.

.. admonition:: Dedication

    For statisticians

.. admonition:: Abstract

    This small study aims to investigate possible ways of application of Monte-Carlo techniques in 
    Option trading. Several addaptive algorithms of the Monte-Carlo simulation and integration are
    considered. 

.. meta::
   :keywords: pandas,ipython,dataframe,trading, statistics, finance
   :description lang=en: Introduction to the practical Monte-Carlo methods in Option trading.




.. contents:: Table of Contents





----------------------------------------------------------------
Introduction to the topic and motivation
----------------------------------------------------------------

I want to introduce you to my motivation which forces me to develop this project.
In mathematical finance, a Monte Carlo option model uses Monte Carlo methods
to calculate the value of an option with multiple sources of uncertainty or with complicated  [wiki_].

An option on equity may be modelled with one source of uncertainty: 
the price of the underlying stock in question.
This price of the underlying instrument is usually modelled such that it follows a geometric Brownian motion 
with constant drift mu  and volatility sigma.

The technique applied then, is  

1. to generate a large number of possible (but random) 
   price paths for the underlying (or underlyings) via simulation, and 
2. to then calculate the associated exercise value (i.e. "payoff") of the option for each path.
3. These payoffs are then averaged and 
4. discounted to today. 


This result is the value of the option.

.. _wiki: https://en.wikipedia.org/wiki/Monte_Carlo_methods_for_option_pricing


----------------------------------
Installation of the tutorial
----------------------------------

.. Important::
  
    The project relies on the presence of ``Autotools`` in the system.
    Before, proceed further, please, install them: ``sudo apt-get install autotools-dev``.


Simply, clone the project 

.. code-block:: bash

    git clone https://iggy_floyd@bitbucket.org/iggy_floyd/monte_carlo_stochastic_process.git

and test a configuration of your system that you have components like
    
* python 

* scientific python modules.

installed. It is done via the ``make``  command as it's  shown below

.. code-block:: bash
    
    make
    
It is convenient to work with the ipython notebook having  plugins for document workflow installed. 
They provide spell checking as TOC and reference tools. 
To get them into the system, just run

.. code-block:: bash

   sudo ipython install-nbextension https://bitbucket.org/ipre/calico/downloads/calico-document-tools-1.0.zip
   sudo ipython install-nbextension https://bitbucket.org/ipre/calico/downloads/calico-spell-check-1.0.zip
   sudo ipython install-nbextension https://bitbucket.org/ipre/calico/downloads/calico-cell-tools-1.0.zip
   sudo ipython install-nbextension https://rawgit.com/jfbercher/latex_envs/master/latex_envs.zip    


----------------------------------------------------------------
Documentation
----------------------------------------------------------------

The main parts of documentation are placed  in the ``README.rst``.
Running the command 

.. code-block:: 
    
    make doc
    
    
will make a folder ``doc/`` with the ``README.html`` which collects all ``*.rst`` files together in
the HTML format.

----------------------------------------------------------------
Samples
----------------------------------------------------------------


The Yahoo! Finance service_ is used to collect data in the tutorial.

.. _service: http://finance.yahoo.com/



----------------------------------------------------------------
References
----------------------------------------------------------------


.. target-notes::
    


