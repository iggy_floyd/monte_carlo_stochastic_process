'''

 Taken from 
 https://github.com/CamDavidsonPilon/Python-Numerics/blob/master/MonteCarlo/Integration/MonteCarloIntegrator.py

 Author CamDavidsonPilon 
 Modified by Igor Marfin


'''

import numpy as np
import scipy.stats as stats
import time

class MCIntegrator( object ):
    """
    target_function: a function that accepts a n-D array, and returns an n-D array.
    interval: the interval of the integration
    b_antithetic: whether to use antithesis variables. Much quicker, but only useful on monotonic target_functions
    sampling_dist: a scipy frozen distribution with support equal to the interval
    N: number of variables to use in the initial estimate.
    control_variates = a list of function that accepts a nD array, and return an nD array
    """
    def __init__(self, target_function, 
                       interval = (0,1), 
                       N = 10000, 
                       b_antithetic = False, 
                       sampling_dist = stats.uniform(), 
                       verbose=False,
                       control_variates = []):
        self.target_function = target_function
        self.min_interval, self.max_interval = interval
        self.N_ = N
        self.N = 0
        self.sampling_dist = sampling_dist
        self.value =0
        self.var=np.nan
        self.b_antithetic = b_antithetic
        self.verbose = verbose
        self.control_variates = control_variates
        self.sample = np.array([])
   
   
    def estimate_N(self, N ):
        self.N += N
        return self._estimate(N)
        
        
        
    def _estimate(self, N):
        
        #generate N values from sampling_dist
        if not self.b_antithetic:
            U = self.sampling_dist.rvs(N)
            Y = self.target_function( U )
            for func in self.control_variates:
                X =  func(U)
                Y += X
                
            if self.verbose:
                print Y.var()
            self.value +=  Y.sum()
        else:
            U_ = self.sampling_dist.rvs(N/2)
            antiU_ = self.min_interval + (self.max_interval - U_ )
            Y =  (self.target_function( U_ ) + self.target_function( antiU_ ) )
            if self.verbose:
                print Y.var()
            self.value +=Y.sum()
        self.sample=np.concatenate((self.sample,Y))
        self.var = self.sample.var(ddof=1)
        return self.value / self.N 
        
    def estimate(self):
        self.N += self.N_
        return self._estimate(self.N_)


    def SE(self):        
        return np.sqrt(self.var/self.N) if self.var is not np.nan else np.nan

